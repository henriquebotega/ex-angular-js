sbm.directive("listActionButtons", function () {
	return {
		restrict: "E",
		transclude: true,
		controller: function ($scope, $element) {
			console.log("directive");
		},
		template: '<div class="row" ng-transclude />',
	};
});
