sbm.controller("Grid", function ($scope) {
	$scope.valor_entrada;
	$scope.valor_multiplicado;
	$scope.valor_saida = 0;

	$scope.calcular = function () {
		if ($scope.valor_entrada > 0 && $scope.valor_multiplicado > 0) {
			$scope.valor_saida = $scope.valor_entrada * $scope.valor_multiplicado;
		}
	};
});
