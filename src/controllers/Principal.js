sbm.controller("Principal", function (
	$scope,
	$location,
	Musicas,
	Util,
	$filter
) {
	$scope.tela = "tela principal";

	Musicas.getRegistros().then((res) => {
		console.log(res.data);

		Util.getItems(res.data);
	});

	$scope.redirecionar = function () {
		$location.path("/editar/2");
	};

	$scope.init = function () {
		console.log($filter(`currency`)(450.0));
	};

	$scope.goToGrid = function () {
		$location.path("/grid");
	};

	$scope.init();
});
