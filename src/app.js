var sbm = angular.module("sbm", [
	"ngResource",
	"ngSanitize",
	"ngTouch",
	"ngAnimate",
	"ngRoute",
	// "ui.bootstrap",
]);

sbm.config(function ($routeProvider) {
	$routeProvider.otherwise("/principal");

	$routeProvider
		.when("/", {
			templateUrl: "src/pages/principal.html",
			controller: "Principal",
		})
		.when("/grid", {
			templateUrl: "src/pages/grid.html",
			controller: "Grid",
		})
		.when("/editar/:id", {
			templateUrl: "src/pages/editar.html",
			controller: "Editar",
		})
		.otherwise("/");
});
