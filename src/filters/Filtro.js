sbm.filter("maiusculo", function () {
	return function (text) {
		if (text) {
			return text.toUpperCase();
		}
	};
});

sbm.filter("brCep", function () {
	return function (cep) {
		var cep = cep.replace(/\W/g, cep);
		const regex = /^(\d{5})(\d{3})$/g;
		return cep.replace(regex, "$1-$2");
	};
});
